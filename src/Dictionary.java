import java.util.Map;

public class Dictionary {

    private Map<String, String> slownik;

    public Dictionary(Map<String, String> slownik) {
        this.slownik = slownik;
    }

    public void addtoDictionary(String polskie, String angielskie) {
        slownik.put(polskie, angielskie);
    }

    public boolean editDictionaryWord(String polskieSlowo, String angielskieSlowo) {
        if (slownik.get(polskieSlowo) == null) {
            slownik.put(polskieSlowo, angielskieSlowo);
            return false;
        } else {
            slownik.put(polskieSlowo, angielskieSlowo);
            return true;
        }
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "slownik=" + slownik +
                '}';
    }
}