public class Main {

    public static void main(String[] args) {

        Shop shop = new Shop();
        Person p1 = new Person("Darek", "W", 33);
        Person p2 = new Person("Marek", "W", 33);
        Person p3 = new Person("Waldek", "W", 33);
        Person p4 = new Person("Ola", "W", 33);

        System.out.println("Limit sklepowej kolejki: "+  shop.LIMIT);
        shop.serveCustomer();

        shop.addToQueue(p1);
        shop.addToQueue(p2);
        shop.addToQueue(p3);

        shop.listQueue();

        shop.serveCustomer();

        shop.listQueue();

        shop.addToQueue(p4);
        shop.listQueue();

    }
}

