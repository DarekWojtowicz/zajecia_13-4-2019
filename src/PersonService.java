import java.util.Map;
import java.util.TreeMap;

public class PersonService {

    Person person;
    int ID;
    Map<Integer, Person> mapa = new TreeMap<Integer, Person>();

    public PersonService(Person person, int ID) {
        this.person = person;
        this.ID = ID;
    }

    public void addPerson(int ID, Person person) {
        this.person = person;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

 }
