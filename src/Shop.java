import java.util.LinkedList;
import java.util.Queue;

public class Shop {
    public static final int LIMIT = 3;
    Queue <Person> kolejka ;

    public Shop(){
        kolejka = new LinkedList<Person>();
    }

    public void addToQueue(Person person){
        if(kolejka.size() < LIMIT) {
            kolejka.add(person);
        }else {
            System.out.println("kolejka pelna!");
        }
     }

    public void serveCustomer(){
        if (kolejka.size()>0){
            kolejka.poll();
        }else {
            System.out.println("Kolejka pusta! Brak klientów do obsłużenia");
        }
    }

    public void listQueue(){
        System.out.println("W kolejce  są:");
        for(Person tmp : kolejka){
            System.out.println(tmp.getImie());
        }
    }
}
